const oracledb = require('oracledb');

oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT;

async function run() {

  let connection;

  try {
    connection = await oracledb.getConnection(  {
      user          : "std_iwf_dev",
      password      : "std_iwf_dev",
      connectString : "192.168.1.16:1521/wforcl"
    });

    const result = await connection.execute(
      `SELECT * FROM iwz_user_master`
    //   [empid]   // bind value for :id
      );
    console.log(result.rows);

  } catch (err) {
    console.error(err);
  } finally {
    if (connection) {
      try {
        await connection.close();
      } catch (err) {
        console.error(err);
      }
    }
  }
}

run();




// SELECT * FROM custom_wm_client_master



// oracledb: [
//         {
//             alias: "oraclePool",
//             user: "std_iwf_dev",
//             password: "std_iwf_dev",
//             connectString: "192.168.1.16:1521/wforcl"
//         }
//     ]





// const oracledb = require('oracledb');
// oracledb.getConnection(
// {  
//     user:"test_demo",
//     password:"test_demo",
//     connectionString:"192.168.1.33:1521/mfxweb"
//   },
//   (err,connection)=>{
//     if(err){
//       console.error(err.message)
//       return;
//     } 
//     // query Execution for selection
//     connection.execute(
//       'select * from dept_master',
//       [],
//     {
//       maxRows:5
//     },
//     (err,result)=>{
//       if(err){
//         console.error(err.message);
//         return;
//       }
//       console.log(result.rows);
//     });
//   });
